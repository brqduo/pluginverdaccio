'use strict';

var ActiveDirectory = require('activedirectory');
var _ = require('lodash');

function Plugin(config, stuff) {
	var self = Object.create(Plugin.prototype);
	self._config = config;
	self._logger = stuff.logger;
	self._logger.info('Active Directory plugin configuration:\n', config);
	return self;
}

Plugin.prototype.authenticate = function (user, password, callback) {
	var self = this;
	var username_config = this._config.username;
	var password_config = this._config.password;
	var groupName = this._config.group;
	var connection = new ActiveDirectory(_.extend(this._config, { username: username_config, password: password_config }));
	connection.on('error', function (error) {
		self._logger.warn('Active Directory connection error. Error:', error);
	});

	connection.isUserMemberOf(user, groupName, function (err, isMember) {
		if (err) {
			self._logger.warn('Active Directory authentication failed. Error code:', err.code + '.', 'Error:\n', err);
			return callback(err);
		} else {
			if (isMember) {
				self._logger.info('Acesso autorizado ao usuário ', user)
				callback(null, [user]);
			} else {
				self._logger.info('Acesso negado ao usuário ', user)
				callback(err);

			}
		}
	});
};

module.exports = Plugin;